## Get-AclLocallyLocal
function Get-AclLocallyLocal_test2 {

 [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [String]$drive,
        [Parameter(Mandatory=$false)]
        [int]$depth 

    )

    BEGIN{
        
    }#BEGIN
            

    PROCESS {
        
        foreach($item in get-childitem -path $drive -ErrorAction SilentlyContinue -ErrorVariable +err){
                
                
                #if is directory
                if ($item.psiscontainer){
                       
                  #if amount of directories are equal or lower than specified depth
                  if(($item.fullname -split '\\').count -le $depth){
                        
                        
                                                                    

                        $aclLocalyHashtable=@{
                            Directory=$item.fullname
                            Error=""
                        }
                        $aclLocallyObject = new-object -TypeName psobject -Property $aclLocalyHashtable
                        $aclLocallyObject   
                        Get-AclLocallyLocal_test2 $item.fullname -depth $depth

                        
                    }    
                    elseif(!($item.fullname -split '\\').count -le $depth){
                        continue
                    }
                }#if psiscontainer    
                else{
                    continue
                }
            
            }#foreach   


    }#PROCESS   

    END{
               
        #$aclLocallyObject
        
    }#END
    
}  

Get-AclLocallyLocal_test2 -drive "C:\temp\CarpetaPrueba" -depth 5
